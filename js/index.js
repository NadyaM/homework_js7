const users1 = [{
    name: "Ivan",
    surname: "Ivanov",
    gender: "male",
    age: 30
},
    {
        name: "Anna",
        surname: "Ivanova",
        gender: "female",
        age: 22
    }]

const users2 = [{
    name: "Vasya",
    surname: "Ivanov",
    gender: "male",
    age: 30
},
    {
        name: "Anna",
        surname: "Petrova",
        gender: "female",
        age: 22
    }]

function filterUser(mass1, mass2, property){
    return mass1.filter(function(elemOfArr){
        return !mass2.some(function(similarElemOfArr){
            return elemOfArr[property] === similarElemOfArr[property];
        })
    })
}
console.log(filterUser(users1,users2,'name'));
    